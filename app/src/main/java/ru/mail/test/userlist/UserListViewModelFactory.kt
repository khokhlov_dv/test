package ru.mail.test.userlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mail.test.data.repository.UserRepository
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class UserListViewModelFactory @Inject constructor(private val userRepository: UserRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserListViewModel::class.java)) {
            return UserListViewModel(userRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}