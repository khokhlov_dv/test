package ru.mail.test.userlist.di

import dagger.Subcomponent
import ru.mail.test.userlist.UserListFragment

@Subcomponent
interface UserListComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(): UserListComponent
    }
    fun inject(fragment: UserListFragment)
}