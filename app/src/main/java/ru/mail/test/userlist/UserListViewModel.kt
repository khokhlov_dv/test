package ru.mail.test.userlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.mail.test.data.domain.User
import ru.mail.test.data.repository.UserRepository

class UserListViewModel(private val userRepository: UserRepository) : ViewModel() {
    val userList = userRepository.userList
    private val _navigateToUserDetail = MutableLiveData<User>()
    val navigateToUserDetail: LiveData<User>
        get() = _navigateToUserDetail

    private val _onSwipeRefreshProcessing = MutableLiveData<Boolean>()
    val onSwipeRefreshProcessing: LiveData<Boolean>
        get() = _onSwipeRefreshProcessing

    init {
        _onSwipeRefreshProcessing.value = false
        refreshUserList()
    }

    private fun refreshUserList() {
        viewModelScope.launch {
            userRepository.refreshUserList()
            if ( _onSwipeRefreshProcessing.value == true ) {
                _onSwipeRefreshProcessing.value = false
            }
        }
    }

    fun onUserClicked(user: User) {
        _navigateToUserDetail.value = user
    }

    fun onUserDetailNavigated() {
        _navigateToUserDetail.value = null
    }

    fun swipeRefresh() {
        _onSwipeRefreshProcessing.value = true
        refreshUserList()
    }
}