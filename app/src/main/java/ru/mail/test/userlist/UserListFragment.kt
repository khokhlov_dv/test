package ru.mail.test.userlist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import ru.mail.test.R
import ru.mail.test.TestApplication
import ru.mail.test.databinding.FragmentUserListBinding
import javax.inject.Inject


class UserListFragment : Fragment() {
    @Inject lateinit var userListViewModelFactory: UserListViewModelFactory
    private val viewModel: UserListViewModel by viewModels {
        userListViewModelFactory
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (requireActivity().application as TestApplication).appComponent
            .userListComponent()
            .create()
            .inject(this)
        val adapter = UserListAdapter(UserClickListener {user ->
            viewModel.onUserClicked(user)
        })
        val binding = FragmentUserListBinding.inflate(inflater,container,false).apply {
            userList.adapter = adapter
            userList.addItemDecoration(DividerItemDecoration(userList.context, LinearLayoutManager.VERTICAL))
            swipeLayout.setOnRefreshListener {
                viewModel.swipeRefresh()
            }
            swipeLayout.setColorSchemeColors(ContextCompat.getColor(context!!, R.color.primaryColor))
        }
        viewModel.userList.observe(viewLifecycleOwner, Observer { users ->
            users?.let {
                adapter.submitList(it)
            }
        })
        viewModel.navigateToUserDetail.observe(viewLifecycleOwner, Observer {user->
            user?.let {
                this.findNavController().navigate(UserListFragmentDirections.actionUserListFragmentToUserDetailFragment(user))
                viewModel.onUserDetailNavigated()
            }
        })
        viewModel.onSwipeRefreshProcessing.observe(viewLifecycleOwner, Observer {  swipeProcessing->
            if (swipeProcessing == false) {
                binding.swipeLayout.isRefreshing = false
            }
        })
        return binding.root
    }
}
