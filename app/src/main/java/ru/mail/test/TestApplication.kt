package ru.mail.test

import android.app.Application
import ru.mail.test.di.AppComponent
import ru.mail.test.di.DaggerAppComponent

class TestApplication : Application() {
    val appComponent: AppComponent by lazy {
        initializeComponent()
    }
    private fun initializeComponent() = DaggerAppComponent.factory().create(applicationContext)
}