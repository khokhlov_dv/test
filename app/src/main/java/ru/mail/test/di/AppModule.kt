package ru.mail.test.di

import android.content.Context
import androidx.room.Room
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import ru.mail.test.data.database.UserDao
import ru.mail.test.data.database.UserDatabase
import ru.mail.test.data.network.BASE_URL
import ru.mail.test.data.network.UserApiService
import javax.inject.Singleton


@Module
class AppModule {
    @Singleton
    @Provides
    fun provideRetrofitService(): UserApiService {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .baseUrl(BASE_URL)
            .build()
            .create(UserApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideDAO(applicationContext: Context):UserDao {
        return Room.databaseBuilder(applicationContext,UserDatabase::class.java, "users_database")
            .fallbackToDestructiveMigration()
            .build()
            .userDao
    }
}