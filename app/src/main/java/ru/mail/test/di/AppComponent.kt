package ru.mail.test.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import ru.mail.test.userdetail.di.UserDetailComponent
import ru.mail.test.userlist.UserListFragment
import ru.mail.test.userlist.di.UserListComponent
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        SubcomponentsModule::class
    ]
)
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }
    fun userListComponent() : UserListComponent.Factory
    fun userDetailComponent() : UserDetailComponent.Factory
}

@Module(
    subcomponents = [
        UserListComponent::class,
        UserDetailComponent::class
    ]
)
object SubcomponentsModule