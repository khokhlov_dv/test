package ru.mail.test.data.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface UserDao {
    @Query("select * from user_list_table")
    fun getUserList(): LiveData<List<DBUserList>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserList(vararg userList: DBUserList)
    @Query("select * from user_info_table where userid = (:userid)")
    fun getUserInfo(userid: Int): LiveData<List<DBUserInfo>>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserInfo(vararg userInfo: DBUserInfo)
}

@Database(entities = [DBUserList::class,DBUserInfo::class] , version = 2, exportSchema = false)
abstract class UserDatabase: RoomDatabase() {
    abstract val userDao: UserDao
}



