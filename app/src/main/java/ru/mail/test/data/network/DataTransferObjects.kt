package ru.mail.test.data.network

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import ru.mail.test.data.database.DBUserInfo
import ru.mail.test.data.database.DBUserList

@JsonClass(generateAdapter = true)
data class NetworkUserList(
    val userid : Int,
    val username : String,
    val department: String,
    val position: String
)

fun List<NetworkUserList>.asDatabaseModel(): Array<DBUserList> = map {
    DBUserList(
        userid = it.userid,
        name = it.username,
        department = it.department,
        position = it.position
    )
}.toTypedArray()

@JsonClass(generateAdapter = true)
data class NetworkUserDetail(
    val userid: Int,
    val info: List<NetworkUserInfo>
) {
    fun asDatabaseModel(): Array<DBUserInfo> = info.map {
        DBUserInfo(
            userid = userid,
            propertyid = it.propertyId,
            property = it.property,
            value = it.value
        )
    }.toTypedArray()
}

data class NetworkUserInfo (
    @Json(name = "property_id") val propertyId : Int,
    val property: String,
    val value: String
)