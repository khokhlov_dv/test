package ru.mail.test.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import ru.mail.test.data.database.UserDao
import ru.mail.test.data.database.asDomainUserInfoModel
import ru.mail.test.data.database.asDomainUserModel
import ru.mail.test.data.domain.User
import ru.mail.test.data.domain.UserInfo
import ru.mail.test.data.network.UserApiService
import ru.mail.test.data.network.asDatabaseModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(private val userDao : UserDao, private val api: UserApiService) {
    val userList: LiveData<List<User>> =
        Transformations.map(userDao.getUserList()) {
            it.asDomainUserModel()
        }
    suspend fun refreshUserList() {
        withContext(Dispatchers.IO) {
            val userList = api.getUserListAsync().await()
            userDao.insertUserList(*userList.asDatabaseModel())
        }
    }
    suspend fun updateUserInfo(userid: Int) {
        withContext(Dispatchers.IO) {
            val userInfo = api.getUserDetailAsync(userid).await()
            userDao.insertUserInfo(*userInfo.asDatabaseModel())
        }
    }
    fun getUserInfo(userid: Int) : LiveData<List<UserInfo>> {
        return Transformations.map(userDao.getUserInfo(userid)) {
            it.asDomainUserInfoModel()
        }
    }
}