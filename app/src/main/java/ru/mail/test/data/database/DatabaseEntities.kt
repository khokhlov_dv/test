package ru.mail.test.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import ru.mail.test.data.domain.User
import ru.mail.test.data.domain.UserInfo

@Entity(tableName = "user_list_table")
data class DBUserList (
    @PrimaryKey
    val userid : Int,
    val name : String,
    val department: String,
    val position : String
)

fun List<DBUserList>.asDomainUserModel(): List<User> = map {
    User(
        userid = it.userid,
        name = it.name,
        department = it.department,
        position = it.position
    )
}

@Entity(tableName = "user_info_table", primaryKeys = ["userid","propertyid"])
data class DBUserInfo (
    val userid: Int,
    val propertyid: Int,
    val property: String,
    val value: String
)

fun List<DBUserInfo>.asDomainUserInfoModel(): List<UserInfo> = map {
    UserInfo(
        propertyId = it.propertyid,
        property = it.property,
        value = it.value
    )
}
