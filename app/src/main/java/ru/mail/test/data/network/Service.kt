package ru.mail.test.data.network

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL = "https://lively-lake-5796.getsandbox.com/"

interface UserApiService {
    @GET("users")
    fun getUserListAsync(): Deferred<List<NetworkUserList>>
    @GET("users/{user_id}")
    fun getUserDetailAsync(@Path(value = "user_id", encoded = true) userId: Int) : Deferred<NetworkUserDetail>
}

