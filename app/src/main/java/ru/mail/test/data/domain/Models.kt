package ru.mail.test.data.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User (
    val userid : Int,
    val name : String,
    val department: String,
    val position : String
) : Parcelable

data class UserInfo(
    val propertyId: Int,
    val property: String,
    val value: String
)