package ru.mail.test.userdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.mail.test.data.domain.User
import ru.mail.test.data.repository.UserRepository
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class UserDetailViewModelFactory @Inject constructor(
    private val userRepository: UserRepository,
    private val user: User
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserDetailViewModel::class.java)) {
            return UserDetailViewModel(userRepository,user) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
