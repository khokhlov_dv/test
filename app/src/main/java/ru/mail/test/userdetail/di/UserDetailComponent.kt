package ru.mail.test.userdetail.di

import dagger.BindsInstance
import dagger.Subcomponent
import ru.mail.test.data.domain.User
import ru.mail.test.userdetail.UserDetailFragment


@Subcomponent
interface UserDetailComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance user: User): UserDetailComponent
    }
    fun inject(fragment: UserDetailFragment)
}