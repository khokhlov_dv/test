package ru.mail.test.userdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.mail.test.data.domain.User
import ru.mail.test.data.repository.UserRepository

class UserDetailViewModel(private val userRepository: UserRepository, user: User) : ViewModel() {
    private val _currentUser = MutableLiveData<User>()
    val userInfo = userRepository.getUserInfo(user.userid)
    val currentUser: LiveData<User>
        get() = _currentUser
    init {
        _currentUser.value = user
        viewModelScope.launch {
            userRepository.updateUserInfo(user.userid)
        }
    }
}