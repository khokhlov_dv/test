package ru.mail.test.userdetail

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.google.android.material.appbar.AppBarLayout
import ru.mail.test.TestApplication
import ru.mail.test.databinding.FragmentUserDetailBinding
import javax.inject.Inject
import kotlin.math.abs

class UserDetailFragment : Fragment() {
    @Inject
    lateinit var userDetailViewModelFactory: UserDetailViewModelFactory
    private val viewModel: UserDetailViewModel by viewModels {
        userDetailViewModelFactory
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val user = UserDetailFragmentArgs.fromBundle(arguments!!).selectedUser
        (requireActivity().application as TestApplication).appComponent
            .userDetailComponent()
            .create(user)
            .inject(this)
        val binding = FragmentUserDetailBinding.inflate(inflater,container,false).apply {
            val adapter = UserInfoAdapter()
            userDetail.adapter = adapter
            userDetail.addItemDecoration(DividerItemDecoration(userDetail.context,LinearLayoutManager.VERTICAL))
            viewModel.userInfo.observe(viewLifecycleOwner, Observer {
                adapter.submitList(it)
            })
            appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener {  appbar, verticalOffset ->
                toolbarLayout.isTitleEnabled = appbar.height - abs(verticalOffset) == toolbar.height
            })
            toolbar.setNavigationOnClickListener {
                it.findNavController().navigateUp()
            }
            userDetailViewModel = viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }
}
