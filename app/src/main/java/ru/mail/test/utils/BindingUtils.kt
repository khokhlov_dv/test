package ru.mail.test.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.android.material.appbar.CollapsingToolbarLayout
import ru.mail.test.data.domain.User
import ru.mail.test.data.domain.UserInfo

@BindingAdapter("userName")
fun TextView.setUserName(item: User?) {
    item?.let {
        text = item.name
    }
}
@BindingAdapter("userDepartment")
fun TextView.setDepartment(item: User?) {
    item?.let {
        text = item.department
    }
}
@BindingAdapter("userPosition")
fun TextView.setPosition(item: User?) {
    item?.let {
        text = item.position
    }
}
@BindingAdapter("userProperty")
fun TextView.setProperty(item: UserInfo?) {
    item?.let {
        text = item.property
    }
}
@BindingAdapter("userPropertyValue")
fun TextView.setPropertyValue(item: UserInfo?) {
    item?.let {
        text = item.value
    }
}

@BindingAdapter("toolbarUserName")
fun CollapsingToolbarLayout.setUserName(item: User?) {
    item?.let {
        title = item.name
    }
}